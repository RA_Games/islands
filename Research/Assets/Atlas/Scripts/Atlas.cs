﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atlas : ScriptableObject {

    [SerializeField] private ProceduralIslandBlueprint generator;
    [SerializeField] private int islandsCount;
    [SerializeField] private string atlasID;

    [SerializeField] private List<IslandData> islands = new List<IslandData>();

    public void Init(ProceduralIslandBlueprint generator, int islandsCount)
    {
        this.generator = generator;
        this.islandsCount = islandsCount;

        atlasID = "#" + Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString();

        Generate();
    }

    [ContextMenu("Generate")]
    void Generate()
    {
        islands.Clear();
        //FREE MEMORY IN SAVES

        List<int> seeds = new List<int>();

        while (seeds.Count < islandsCount)
        {
            int seed = Random.Range(0, Random.Range(5, Random.Range(10,20)));

            bool found = false;

            for (int i = 0; i < seeds.Count; i++)
            {
                if(seeds[i] == seed)
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                seeds.Add(seed);
            }
        }

        foreach (int seed in seeds)
        {
            islands.Add(generator.GenerateIslandData(seed));
        }
    }
}
