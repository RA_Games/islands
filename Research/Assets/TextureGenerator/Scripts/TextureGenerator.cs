﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using LibNoise.Generator;

public static class TextureGenerator {

    public static Texture2D Solid(int width, int height, Color color)
    {
        Texture2D result = new Texture2D(width, height, TextureFormat.RGBA32, true);

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                result.SetPixel(x,y, color);
            }
        }

        return result;
    }

    public static Texture2D SetGrayscale(Texture2D input, float grayscale)
    {
        Texture2D output = input;

        for (int y = 0; y < output.height; y++)
        {
            for (int x = 0; x < output.width; x++)
            {
                output.SetPixel(x, y, new Color(input.GetPixel(x,y).r, input.GetPixel(x, y).g, input.GetPixel(x,y).b, 1));
            }
        }

        return output;
    }

    public static Texture2D RorschachNoise(Texture2D perlin, float tolerance = 0.5f, int sample = 1)
    {
        Texture2D output = Solid(perlin.width, perlin.height, Color.black);

        for (int y = 1; y < perlin.height - 1; y++)
        {
            for (int x = 1; x < perlin.width - 1; x++)
            {
                float whiteInGrid = 0;

                for (int pixelY = y - sample; pixelY <= y + sample; pixelY++)
                {
                    for (int pixelX = x - sample; pixelX <= x + sample; pixelX++)
                    {
                        whiteInGrid += perlin.GetPixel(pixelX, pixelY).grayscale;
                    }
                }

                whiteInGrid /= ((((sample*sample) + sample)/2)*8);

                if (whiteInGrid > tolerance)
                {
                    output.SetPixel(x, y, Color.white);
                }
                else
                {
                    output.SetPixel(x, y, Color.black);
                }

                
            }
        }

        return output;
    }

    public static Texture2D NoisePoint(int width, int height, float radius = 10 ,int offsetX = 0, int offsetY = 0)
    {
        Texture2D output = Solid(width, height, Color.black);

        Vector2 center = new Vector2((width*0.5f)+offsetX, (height*0.5f)+offsetY);

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                float distance = Vector2.Distance(center, new Vector2(x,y));
                float probability = distance / radius;

                if (Random.Range(0.25f,1f) > probability)
                {
                    output.SetPixel(x,y, Color.white);
                }
            }
        }

        return output;
    }

    public static Texture2D Perlin(int width, int height, float frequency, float lacunarity, float persistence, int octaves, int seed)
    {
        Texture2D result = new Texture2D(width, height);

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {

                Perlin perlin = new Perlin(frequency, lacunarity, persistence, octaves, seed, LibNoise.QualityMode.High);

                float value = (float)perlin.GetValue(x, y, 0.5f);

                Color color = new Color(value, value, value);

                result.SetPixel(x, y, color);
            }
        }

        return result;
    }

    public static Texture2D CircularGradient(int width, int height, float maskThreshold = 2, float radius = 0.5f, int offsetX = 0, int offsetY = 0)
    {
        Texture2D mask = new Texture2D(width, height, TextureFormat.RGBA32, true);
        Vector2 maskCenter = new Vector2((width*0.5f)+offsetX, (height * 0.5f) + offsetY);

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {

                float distFromCenter = Vector2.Distance(maskCenter, new Vector2(x, y));
                float maskPixel = (radius - (distFromCenter / width)) * maskThreshold;
                mask.SetPixel(x, y, new Color(maskPixel, maskPixel, maskPixel, 1.0f));
            }
        }
        mask.Apply();

        return mask;
    }

    public static Texture2D Voronoi(int width, int height, int spots = 10, float magnitude = 2)
    {
        Texture2D result = Solid(width, height, Color.black);

        Vector2[] spotCoords = new Vector2[spots];

        for (int i = 0; i < spots; i++)
        {
            spotCoords[i] = new Vector2(width * Random.Range(0f,1f), height*Random.Range(0f,1f));
        }

        for (int y = 0; y < result.height; y++)
        {
            for (int x = 0; x < result.width; x++)
            {
                Vector2 nearSpot = spotCoords[0];
                float nearDistance = width+height;
                Vector2 pixelCoord = new Vector2(x,y);

                for (int i = 1; i < spots; i++)
                {
                    float pixelCoordDistance = Vector2.Distance(pixelCoord, spotCoords[i]);
                    if (pixelCoordDistance < nearDistance)
                    {
                        nearDistance = pixelCoordDistance;
                        nearSpot = spotCoords[i];
                    }
                }

                float distance = (Vector2.Distance(pixelCoord, nearSpot) / width) * magnitude;

                Color color = new Color(distance, distance, distance);

                result.SetPixel(x,y,color);
            }

            //return result;
        }

        return result;
    }

    public static Texture2D Intersection(Texture2D texture1, Texture2D texture2)
    {
        Texture2D result = Solid(texture1.width, texture1.height, Color.black);

        if(texture1.width == texture2.width && texture1.height == texture2.height)
        {
            for (int y = 0; y < result.height; y++)
            {
                for (int x = 0; x < result.width; x++)
                {
                    Color mult = texture1.GetPixel(x, y) * texture2.GetPixel(x, y);

                    result.SetPixel(x,y,mult);
                }
            }

            return result;
        }
        else
        {
            Debug.Log("Both textures require have the same resolution in order to composite!");
            return result;
        }
    }

    public static Texture2D Union(Texture2D texture1, Texture2D texture2)
    {
        Texture2D result = Solid(texture1.width, texture1.height, Color.black);

        if (texture1.width == texture2.width && texture1.height == texture2.height)
        {
            for (int y = 0; y < result.height; y++)
            {
                for (int x = 0; x < result.width; x++)
                {
                    float value = texture1.GetPixel(x, y).grayscale + texture2.GetPixel(x, y).grayscale;
                    Color color = new Color(value, value, value, value);

                    result.SetPixel(x, y, color);
                }
            }

            return result;
        }
        else
        {
            Debug.Log("Both textures require have the same resolution in order to composite!");
            return result;
        }
    }

    public static Texture2D Diference(Texture2D texture1, Texture2D texture2)
    {
        Texture2D result = Solid(texture1.width, texture1.height, Color.black);

        if (texture1.width == texture2.width && texture1.height == texture2.height)
        {
            for (int y = 0; y < result.height; y++)
            {
                for (int x = 0; x < result.width; x++)
                {
                    float value = texture1.GetPixel(x, y).grayscale - texture2.GetPixel(x, y).grayscale;
                    Color color = new Color(value, value, value, value);

                    result.SetPixel(x, y, color);
                }
            }

            return result;
        }
        else
        {
            Debug.Log("Both textures require have the same resolution in order to composite!");
            return result;
        }
    }

    public static Texture2D ConvertAlpha(Texture2D texture1)
    {
        for (int y = 0; y < texture1.height; y++)
        {
            for (int x = 0; x < texture1.width; x++)
            {
                if (texture1.GetPixel(x,y).grayscale == 0)
                {
                    texture1.SetPixel(x, y, Color.clear);
                }
            }
        }

        return texture1;
    }
}
 
public class TextureScale
{
    public class ThreadData
    {
        public int start;
        public int end;
        public ThreadData(int s, int e)
        {
            start = s;
            end = e;
        }
    }

    private static Color[] texColors;
    private static Color[] newColors;
    private static int w;
    private static float ratioX;
    private static float ratioY;
    private static int w2;
    private static int finishCount;
    private static Mutex mutex;

    public static void Point(Texture2D tex, int newWidth, int newHeight)
    {
        ThreadedScale(tex, newWidth, newHeight, false);
    }

    public static void Bilinear(Texture2D tex, int newWidth, int newHeight)
    {
        ThreadedScale(tex, newWidth, newHeight, true);
    }

    private static void ThreadedScale(Texture2D tex, int newWidth, int newHeight, bool useBilinear)
    {
        texColors = tex.GetPixels();
        newColors = new Color[newWidth * newHeight];
        if (useBilinear)
        {
            ratioX = 1.0f / ((float)newWidth / (tex.width - 1));
            ratioY = 1.0f / ((float)newHeight / (tex.height - 1));
        }
        else
        {
            ratioX = ((float)tex.width) / newWidth;
            ratioY = ((float)tex.height) / newHeight;
        }
        w = tex.width;
        w2 = newWidth;
        var cores = Mathf.Min(SystemInfo.processorCount, newHeight);
        var slice = newHeight / cores;

        finishCount = 0;
        if (mutex == null)
        {
            mutex = new Mutex(false);
        }
        if (cores > 1)
        {
            int i = 0;
            ThreadData threadData;
            for (i = 0; i < cores - 1; i++)
            {
                threadData = new ThreadData(slice * i, slice * (i + 1));
                ParameterizedThreadStart ts = useBilinear ? new ParameterizedThreadStart(BilinearScale) : new ParameterizedThreadStart(PointScale);
                Thread thread = new Thread(ts);
                thread.Start(threadData);
            }
            threadData = new ThreadData(slice * i, newHeight);
            if (useBilinear)
            {
                BilinearScale(threadData);
            }
            else
            {
                PointScale(threadData);
            }
            while (finishCount < cores)
            {
                Thread.Sleep(1);
            }
        }
        else
        {
            ThreadData threadData = new ThreadData(0, newHeight);
            if (useBilinear)
            {
                BilinearScale(threadData);
            }
            else
            {
                PointScale(threadData);
            }
        }

        tex.Resize(newWidth, newHeight);
        tex.SetPixels(newColors);
        tex.Apply();

        texColors = null;
        newColors = null;
    }

    public static void BilinearScale(System.Object obj)
    {
        ThreadData threadData = (ThreadData)obj;
        for (var y = threadData.start; y < threadData.end; y++)
        {
            int yFloor = (int)Mathf.Floor(y * ratioY);
            var y1 = yFloor * w;
            var y2 = (yFloor + 1) * w;
            var yw = y * w2;

            for (var x = 0; x < w2; x++)
            {
                int xFloor = (int)Mathf.Floor(x * ratioX);
                var xLerp = x * ratioX - xFloor;
                newColors[yw + x] = ColorLerpUnclamped(ColorLerpUnclamped(texColors[y1 + xFloor], texColors[y1 + xFloor + 1], xLerp),
                                                       ColorLerpUnclamped(texColors[y2 + xFloor], texColors[y2 + xFloor + 1], xLerp),
                                                       y * ratioY - yFloor);
            }
        }

        mutex.WaitOne();
        finishCount++;
        mutex.ReleaseMutex();
    }

    public static void PointScale(System.Object obj)
    {
        ThreadData threadData = (ThreadData)obj;
        for (var y = threadData.start; y < threadData.end; y++)
        {
            var thisY = (int)(ratioY * y) * w;
            var yw = y * w2;
            for (var x = 0; x < w2; x++)
            {
                newColors[yw + x] = texColors[(int)(thisY + ratioX * x)];
            }
        }

        mutex.WaitOne();
        finishCount++;
        mutex.ReleaseMutex();
    }

    private static Color ColorLerpUnclamped(Color c1, Color c2, float value)
    {
        return new Color(c1.r + (c2.r - c1.r) * value,
                          c1.g + (c2.g - c1.g) * value,
                          c1.b + (c2.b - c1.b) * value,
                          c1.a + (c2.a - c1.a) * value);
    }
}
