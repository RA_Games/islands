﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody), typeof(MeshFilter))]
public class ThirdPersonController : MonoBehaviour
{
    public float movementSpeed = 35;
    public float rotationSpeed = 10;
    public float jumpForce = 250;

    public float speedLimit = 5;

    public LayerMask whatIsFloor;

    Rigidbody rb;
    Mesh playerModel;

    [SerializeField]bool isTouchingFloor = true;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerModel = GetComponent<MeshFilter>().sharedMesh;
    }

    private void Update()
    {
        Vector3 direction = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

        Vector3 movementX = Camera.main.transform.right * direction.x;
        Vector3 movementZ = (Quaternion.Euler(new Vector3(0, Camera.main.transform.eulerAngles.y, 0)) * Vector3.forward) * direction.z;
        Vector3 movement = movementX + movementZ;

        if (rb.velocity.magnitude <= speedLimit)
        {
            rb.AddForce(movement * movementSpeed);
        }

        if (direction != Vector3.zero)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0,Camera.main.transform.rotation.eulerAngles.y,0)), Time.deltaTime * rotationSpeed);
        }

        if (Input.GetButtonDown("Jump") && isTouchingFloor)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Force);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (whatIsFloor.value == (whatIsFloor.value | (1 << collision.gameObject.layer)))
        {
            isTouchingFloor = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (whatIsFloor.value == (whatIsFloor.value | (1 << collision.gameObject.layer)))
        {
            isTouchingFloor = true;
        }
    }
}
