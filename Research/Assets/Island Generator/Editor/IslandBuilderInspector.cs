﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(IslandBuilder))]
public class IslandBuilderInspector : Editor {

    struct IslandSeed
    {
        public Vector2 coord;
        public float radius;
    }

    IslandBuilder myTarget;

    private void OnEnable()
    {
        myTarget = target as IslandBuilder;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Generate"))
        {
            //Calculating the island seedpoints
            int remainingSeeds = (int)(8*myTarget.size + 15);

            //float islandRadius = (((int)myTarget.resolution) * myTarget.size * 0.45f) + 0.1f * (int)myTarget.resolution;
            float islandRadius = ((int)myTarget.resolution) * 0.6f;

            List<IslandSeed> islandsSeeds = new List<IslandSeed>();

            while (remainingSeeds > 0)
            {
                IslandSeed seed = new IslandSeed();

                int halfResolution = -((int)myTarget.resolution / 2);

                int pointX = Random.Range(-halfResolution, halfResolution);
                int pointY = Random.Range(-halfResolution, halfResolution);

                seed.coord = new Vector2(pointX, pointY);
                seed.radius = Random.Range(0.2f,0.5f) * islandRadius;

                //Check if the point is inside the radius of the island size 
                if (Vector2.Distance(new Vector2(pointX, pointY), Vector2.zero) < islandRadius)
                {
                    //Check if the radius of the seed is inside island radius
                    if ((Vector2.Distance(Vector2.zero, seed.coord) + seed.radius) < islandRadius)
                    {
                        islandsSeeds.Add(seed);
                     //   Debug.Log("Seed in coord: " + seed.coord + " With radius: " + seed.radius);
                        remainingSeeds--;
                    }
                }
            }

            // === Generate top island ===
            List<Texture2D> topLayers = GenerateTopLayers(new List<IslandSeed>(islandsSeeds));
            List<Texture2D> bottomLayers = GenerateBottomLayers(new List<IslandSeed>(islandsSeeds));

            //replaced with generateTopLayers();

            string folderID = "#"+Random.Range(0,9).ToString()+ Random.Range(0, 9).ToString()+ Random.Range(0, 9).ToString() + Random.Range(0, 9).ToString();

            IslandHeightData islandData = CreateInstance<IslandHeightData>();
            AssetDatabase.CreateAsset(islandData, "Assets/Island Generator/Islands/Island_" + folderID + ".asset");

            for (int i = 0; i < topLayers.Count; i++)
            {
                AssetDatabase.AddObjectToAsset(topLayers[i],islandData);
            }

            for (int i = 0; i < bottomLayers.Count; i++)
            {
                AssetDatabase.AddObjectToAsset(bottomLayers[i], islandData);
            }

            islandData.SetTopLayers(topLayers.ToArray());
            islandData.SetBottomLayers(bottomLayers.ToArray());

            List<MeshFilter> meshes = new List<MeshFilter>();

            meshes.AddRange(BuildIslandBottom(islandData));
            meshes.AddRange(BuildIslandTop(islandData));

            CombineInstance[] combine = new CombineInstance[meshes.Count];
            MeshFilter islandRoot = new GameObject("Island_" + folderID, typeof(MeshRenderer), typeof(MeshFilter)).GetComponent<MeshFilter>();

            for (int i = 0; i < meshes.Count; i++)
            {
                combine[i].mesh = meshes[i].sharedMesh;
                combine[i].transform = meshes[i].transform.localToWorldMatrix;
                meshes[i].gameObject.SetActive(false);
            }

            islandRoot.sharedMesh = new Mesh();
            islandRoot.sharedMesh.CombineMeshes(combine);

            AssetDatabase.AddObjectToAsset(islandRoot.mesh, islandData);

            islandData.topology_mesh = islandRoot.mesh;

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = islandData;
        }
    }

    private List<Texture2D> GenerateTopLayers(List<IslandSeed> islandsSeeds)
    {
        List<Texture2D> heighLayers = new List<Texture2D>();

        int layer = 0;
        do
        {
            Texture2D island = TextureGenerator.Solid(((int)myTarget.resolution), ((int)myTarget.resolution), Color.black);

            int validRadius = islandsSeeds.Count;
            for (int i = 0; i < islandsSeeds.Count; i++)
            {
                IslandSeed seed = islandsSeeds[i];

                if (seed.radius <= 15)
                {
                    validRadius--;
                    continue;
                }

                //Calculate the min an max limits of the radius
                float min = seed.radius * ((0.1f) * myTarget.accidentFactor + 0.8f);
                float max = seed.radius * ((0.1f) * myTarget.accidentFactor + 0.9f);
                float radius = Random.Range(min, max); //Calculate the next random radius

                //  radius = (-radius * (layer / myTarget.maxLayers)) + radius; 

                seed.radius = radius;
                int resolution = ((int)myTarget.resolution);

                Texture2D noise = TextureGenerator.NoisePoint(resolution, resolution, radius, (int)seed.coord.x, (int)seed.coord.y);
                island = TextureGenerator.Union(island, noise);
                islandsSeeds[i] = seed;
            }

            if (validRadius == 0)
                break;

            //Octaves
            int octaves = (int)((-4 * myTarget.accidentFactor) + 6);

            for (int i = 0; i < octaves; i++)
            {
                island = TextureGenerator.RorschachNoise(island, 0.7f, ((int)(myTarget.size) + 2));
            }

            if (layer != 0)
            {
                Texture2D excess = TextureGenerator.Diference(island, heighLayers[layer - 1]);
                island = TextureGenerator.Diference(island, excess);
            }

            heighLayers.Add(island);

            layer++;
        } while (layer < myTarget.maxLayers);

        return heighLayers;
    }

    private List<Texture2D> GenerateBottomLayers(List<IslandSeed> islandsSeeds)
    {
        List<Texture2D> heighLayers = new List<Texture2D>();

        int layer = 0;
        do
        {
            Texture2D island = TextureGenerator.Solid(((int)myTarget.resolution), ((int)myTarget.resolution), Color.black);

            int validRadius = islandsSeeds.Count;
            for (int i = 0; i < islandsSeeds.Count; i++)
            {
                IslandSeed seed = islandsSeeds[i];

                if (seed.radius <= 15)
                {
                    validRadius--;
                    continue;
                }

                //Calculate the min an max limits of the radius
                float min = seed.radius * 0.9f;
                float max = seed.radius * 0.95f;
                float radius = Random.Range(min, max); //Calculate the next random radius

                //  radius = (-radius * (layer / myTarget.maxLayers)) + radius; 

                seed.radius = radius;
                int resolution = ((int)myTarget.resolution);

                Texture2D noise = TextureGenerator.NoisePoint(resolution, resolution, radius, (int)seed.coord.x, (int)seed.coord.y);
                island = TextureGenerator.Union(island, noise);
                islandsSeeds[i] = seed;
            }

            if (validRadius == 0)
                break;

            //Octaves
            int octaves = (int)((-4) + 6);

            for (int i = 0; i < octaves; i++)
            {
                island = TextureGenerator.RorschachNoise(island, 0.8f, ((int)(myTarget.size) + 2));
            }

            if (layer != 0)
            {
                Texture2D excess = TextureGenerator.Diference(island, heighLayers[layer - 1]);
                island = TextureGenerator.Diference(island, excess);
            }

            heighLayers.Add(island);

            layer++;
        } while (layer < myTarget.maxLayers*2);

        return heighLayers;
    }

    private List<MeshFilter> BuildIslandTop(IslandHeightData islandData)
    {
        List<MeshFilter> meshes = new List<MeshFilter>();
        GameObject builder = Resources.Load<GameObject>("Island Generator/Layer");

        for (int i = 0; i < islandData.GetTopLayers().Length; i++)
        {
            Texture2D output = TextureGenerator.ConvertAlpha(islandData.GetTopLayers()[i].layer);

            MeshCreatorData layerGo = Instantiate(builder).GetComponent<MeshCreatorData>();
            layerGo.outlineTexture = output;
            layerGo.meshDepth = 1;//(2*((myTarget.layers + 1) - o)) * 0.5f;

            Vector3 pos = layerGo.transform.position;
            pos.y = i;
            //pos.y -= ((((myTarget.layers + 1)-o)*((myTarget.layers + 1)-o)) - (2*((myTarget.layers + 1) - o)))/ 2;

            layerGo.transform.position = pos;

            MeshCreator.UpdateMesh(layerGo.gameObject);

            meshes.Add(layerGo.gameObject.GetComponent<MeshFilter>());
        }

        return meshes;
    }

    private List<MeshFilter> BuildIslandBottom(IslandHeightData islandData)
    {
        List<MeshFilter> meshes = new List<MeshFilter>();
        GameObject builder = Resources.Load<GameObject>("Island Generator/Layer");

        for (int i = 0; i < islandData.GetBottomLayers().Length; i++)
        {
            Texture2D output = TextureGenerator.ConvertAlpha(islandData.GetBottomLayers()[i].layer);

            MeshCreatorData layerGo = Instantiate(builder).GetComponent<MeshCreatorData>();
            layerGo.outlineTexture = output;
            layerGo.meshDepth = 1;//(2*((myTarget.layers + 1) - o)) * 0.5f;

            Vector3 pos = layerGo.transform.position;
            pos.y = -(i+1);
            //pos.y -= ((((myTarget.layers + 1)-o)*((myTarget.layers + 1)-o)) - (2*((myTarget.layers + 1) - o)))/ 2;

            layerGo.transform.position = pos;

            MeshCreator.UpdateMesh(layerGo.gameObject);
            meshes.Add(layerGo.gameObject.GetComponent<MeshFilter>());
        }

        return meshes;
    }
}
