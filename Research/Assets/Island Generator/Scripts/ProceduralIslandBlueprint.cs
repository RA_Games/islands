﻿using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "World/Procedural Blueprint")]
public class ProceduralIslandBlueprint : ScriptableObject
{
    public AnimationCurve temperatureCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve accidentFactorCurve = AnimationCurve.Linear(0, 0, 1, 1);

    public AnimationCurve sizeTempCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve sizeAccidenFactCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve oresSizeCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve oresAccidentFactCurve = AnimationCurve.Linear(0, 0, 1, 1);

    public AnimationCurve floraHumidityCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve floraOresCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve floraAccidentFactCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve faunaFloraCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve faunaTempCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve faunaSizeCurve = AnimationCurve.Linear(0, 0, 1, 1);

    public AnimationCurve inteligenceOresCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve inteligenceSizeCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve inteligenceFloraCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve inteligenceTempCurve = AnimationCurve.Linear(0, 0, 1, 1);

    public Texture2D humidityTexture;

    public float yaw;
    public float pitch;

    public IslandData GenerateIslandData(int seed)
    {
        Random.InitState(seed);

        //Primigenial values
        yaw = Random.Range(0f, 360f);
        pitch = Random.Range(0f, 360f);

        float temperature = temperatureCurve.Evaluate(Random.Range(0f, 1f));
        float accidentFactor = accidentFactorCurve.Evaluate(Random.Range(0f, 1f));
        float humidity = CalculateHumidity(humidityTexture, temperature, yaw);

        //Secondary Values
        float size = (sizeTempCurve.Evaluate(temperature) + sizeAccidenFactCurve.Evaluate(accidentFactor)) / 2;
        float ores = (oresSizeCurve.Evaluate(size) + oresAccidentFactCurve.Evaluate(accidentFactor)) / 2;

        //Terciary
        float flora = (floraHumidityCurve.Evaluate(humidity) + floraOresCurve.Evaluate(ores) + floraAccidentFactCurve.Evaluate(accidentFactor)) / 3;
        float fauna = (faunaFloraCurve.Evaluate(flora) + faunaSizeCurve.Evaluate(size) + faunaTempCurve.Evaluate(temperature)) / 3;

        //Cuaternary
        float inteligence = (inteligenceOresCurve.Evaluate(ores) + inteligenceFloraCurve.Evaluate(flora) + inteligenceSizeCurve.Evaluate(size) + inteligenceTempCurve.Evaluate(temperature)) / 4;

        //Calculate Events
        float ruins = 0, anomalies = 0;

        IslandData data = new IslandData(temperature, accidentFactor, humidity, size, flora, fauna, inteligence, ores, ruins, anomalies, yaw, pitch);

        return data;
    }

    private float CalculateHumidity(Texture2D texture, float temperature, float yaw)
    {
        Vector2 textCoord = Vector3.zero;

        if (yaw <= 180)
        {
            float coord = Mathf.Sin(yaw) * temperature * texture.width;
            textCoord = new Vector2(coord, coord);

        }else if (yaw > 180 && yaw <= 270)
        {
            float coord = -Mathf.Cos(yaw) * temperature * texture.width;
            textCoord = new Vector2(coord, coord);
        }
        else
        {
            float coord = Mathf.Cos(yaw) * temperature * texture.width;
            textCoord = new Vector2(coord, coord);
        }

        return texture.GetPixel((int)textCoord.x, (int)textCoord.y).grayscale;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(ProceduralIslandBlueprint))]
public class ProceduralIslandBlueprintEditor : Editor
{
    ProceduralIslandBlueprint myTarget;
    int seed;
    int atlasSize;
    int testSize = 10;

    AnimationCurve fetchData;
    AnimationCurve islandData;

    enum TestData
    {
        Temperature, AccidentFactor, Humidity, Size, Flora, Fauna, Inteligence, Ores, Ruins, Anomalies
    }

    TestData testData;

    private void OnEnable()
    {
        myTarget = (ProceduralIslandBlueprint)target;
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("Primigenial");
        myTarget.temperatureCurve = EditorGUILayout.CurveField("Temperature", myTarget.temperatureCurve, GUILayout.Height(35));
        myTarget.accidentFactorCurve = EditorGUILayout.CurveField("Accident Factor", myTarget.accidentFactorCurve, GUILayout.Height(35));
        myTarget.humidityTexture = (Texture2D)EditorGUILayout.ObjectField("Humidity", myTarget.humidityTexture, typeof(Texture2D), false);

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Secondary");
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Size: Temperature, Accident Fact", GUILayout.Width(220));
        myTarget.sizeTempCurve = EditorGUILayout.CurveField(myTarget.sizeTempCurve, GUILayout.Height(35));
        myTarget.sizeAccidenFactCurve = EditorGUILayout.CurveField(myTarget.sizeAccidenFactCurve, GUILayout.Height(35));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Ores: Size, Accident Fact", GUILayout.Width(220));
        myTarget.oresSizeCurve = EditorGUILayout.CurveField(myTarget.oresSizeCurve, GUILayout.Height(35));
        myTarget.oresAccidentFactCurve = EditorGUILayout.CurveField(myTarget.oresAccidentFactCurve, GUILayout.Height(35));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Terciary");
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Flora: Humidity, Ores, Accident Fact", GUILayout.Width(180));
        myTarget.floraHumidityCurve = EditorGUILayout.CurveField(myTarget.floraHumidityCurve, GUILayout.Height(35));
        myTarget.floraOresCurve = EditorGUILayout.CurveField(myTarget.floraOresCurve, GUILayout.Height(35));
        myTarget.floraAccidentFactCurve = EditorGUILayout.CurveField(myTarget.floraAccidentFactCurve, GUILayout.Height(35));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Fauna: Flora, Temperature, Size", GUILayout.Width(200));
        myTarget.faunaFloraCurve = EditorGUILayout.CurveField(myTarget.faunaFloraCurve, GUILayout.Height(35));
        myTarget.faunaTempCurve = EditorGUILayout.CurveField(myTarget.faunaTempCurve, GUILayout.Height(35));
        myTarget.faunaSizeCurve = EditorGUILayout.CurveField(myTarget.faunaSizeCurve, GUILayout.Height(35));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Cuaternary");
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Inteligence: Ores, Flora, Size, Temperature", GUILayout.Width(150));
        myTarget.inteligenceOresCurve = EditorGUILayout.CurveField(myTarget.inteligenceOresCurve, GUILayout.Height(35));
        myTarget.inteligenceFloraCurve = EditorGUILayout.CurveField(myTarget.inteligenceFloraCurve, GUILayout.Height(35));
        myTarget.inteligenceSizeCurve = EditorGUILayout.CurveField(myTarget.inteligenceSizeCurve, GUILayout.Height(35));
        myTarget.inteligenceTempCurve = EditorGUILayout.CurveField(myTarget.inteligenceTempCurve, GUILayout.Height(35));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();

        seed = EditorGUILayout.IntField("Seed", seed);

        if (GUILayout.Button("Preview"))
        {
            GameObject island = new GameObject("Island", typeof(Island));
            island.GetComponent<Island>().data = myTarget.GenerateIslandData(seed);
        }

        if (GUILayout.Button("Data"))
        {
           IslandData data = myTarget.GenerateIslandData(seed);

            islandData = AnimationCurve.Linear(0,data.Temperature,9,data.Anomalies);

            islandData.AddKey(1, data.AccidentFactor);
            islandData.AddKey(2, data.Humidity);
            islandData.AddKey(3, data.Size);
            islandData.AddKey(4, data.Ores);
            islandData.AddKey(5, data.Flora);
            islandData.AddKey(6, data.Fauna);
            islandData.AddKey(7, data.Inteligence);
            islandData.AddKey(8, data.Ruins);
        }

        EditorGUILayout.EndHorizontal();

        if (islandData != null)
        {
            EditorGUILayout.CurveField(islandData, GUILayout.Height(40));
        }


        EditorGUILayout.BeginHorizontal();

        testSize = EditorGUILayout.IntField("Test Number", testSize);

        testData = (TestData)EditorGUILayout.EnumPopup(testData);

        List<float> fetchedData = new List<float>();

        if (GUILayout.Button("Stadistics"))
        {
            fetchedData.Clear();

            for (int i = 0; i < testSize; i++)
            {
                IslandData island = myTarget.GenerateIslandData(Random.Range(0,1000));

                switch (testData)
                {
                    case TestData.Temperature:
                        fetchedData.Add(island.Temperature);
                        break;
                    case TestData.AccidentFactor:
                        fetchedData.Add(island.AccidentFactor);
                        break;
                    case TestData.Humidity:
                        fetchedData.Add(island.Humidity);
                        break;
                    case TestData.Size:
                        fetchedData.Add(island.Size);
                        break;
                    case TestData.Flora:
                        fetchedData.Add(island.Flora);
                        break;
                    case TestData.Fauna:
                        fetchedData.Add(island.Fauna);
                        break;
                    case TestData.Inteligence:
                        fetchedData.Add(island.Inteligence);
                        break;
                    case TestData.Ores:
                        fetchedData.Add(island.Ores);
                        break;
                    case TestData.Ruins:
                        fetchedData.Add(island.Ruins);
                        break;
                    case TestData.Anomalies:
                        fetchedData.Add(island.Anomalies);
                        break;
                }

                fetchedData.Sort();
            }

            fetchData = AnimationCurve.Linear(0, fetchedData[0], testSize-1, fetchedData[fetchedData.Count-1]);

            for (int i = 1; i < fetchedData.Count-1; i++)
            {
                fetchData.AddKey(i, fetchedData[i]);
            }
        }

        EditorGUILayout.EndHorizontal();

        if (fetchData != null)
        {
            EditorGUILayout.CurveField(fetchData, GUILayout.Height(40));
        }

        EditorGUILayout.BeginHorizontal();

        atlasSize = EditorGUILayout.IntField("Islands Number", atlasSize);

        if (GUILayout.Button("Generate Atlas"))
        {
            string fileName = "Atlas";
            var path = EditorUtility.SaveFilePanelInProject(
                "Save Atlas",
                fileName,
                "asset",
                "Saving Atlas asset");

            if (path.Length != 0)
            {
                Atlas atlas = CreateInstance<Atlas>();
                atlas.Init(myTarget, atlasSize);

                AssetDatabase.CreateAsset(atlas, path);

                AssetDatabase.SaveAssets();
                Selection.activeObject = atlas;
            }
        }

        EditorGUILayout.EndHorizontal();

        if (GUI.changed)
        {
            EditorUtility.SetDirty(myTarget);
        }
    }
}
#endif