﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class Biomizer : MonoBehaviour
{
    public Gradient top_Colors;

    public IslandData islandData;
    public Texture2D texture;

    [ContextMenu("Biomize")]
    public void Biomize()
    {
        Material island_mat = new Material(Shader.Find("Standard"));

        Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
        Vector3[] normals = GetComponent<MeshFilter>().sharedMesh.normals;
        Vector2[] uvs = GetComponent<MeshFilter>().sharedMesh.uv;

        Vector3 maxPoint = mesh.bounds.max;
        Vector3 minPoint = mesh.bounds.min;
        float islandHeight = Vector3.Distance(new Vector3(mesh.bounds.center.x, maxPoint.y, mesh.bounds.center.z), new Vector3(mesh.bounds.center.x, minPoint.y, mesh.bounds.center.z));

        for (int i = 0; i < mesh.triangles.Length; i+=3)
        {
            float distance_a = Vector3.Distance(mesh.vertices[mesh.triangles[i + 0]], new Vector3(mesh.vertices[mesh.triangles[i + 0]].x, maxPoint.y, mesh.vertices[mesh.triangles[i + 0]].z));
            float distance_b = Vector3.Distance(mesh.vertices[mesh.triangles[i + 1]], new Vector3(mesh.vertices[mesh.triangles[i + 1]].x, maxPoint.y, mesh.vertices[mesh.triangles[i + 1]].z));
            float distance_c = Vector3.Distance(mesh.vertices[mesh.triangles[i + 2]], new Vector3(mesh.vertices[mesh.triangles[i + 2]].x, maxPoint.y, mesh.vertices[mesh.triangles[i + 2]].z));

            Color color_a = top_Colors.Evaluate((distance_a / (islandHeight)));
            Color color_b = top_Colors.Evaluate((distance_b / (islandHeight)));
            Color color_c = top_Colors.Evaluate((distance_c / (islandHeight)));

            Vector2Int uv_a = new Vector2Int((int)(uvs[mesh.triangles[i + 0]].x * texture.width), (int)(uvs[mesh.triangles[i + 0]].y * texture.height));
            Vector2Int uv_b = new Vector2Int((int)(uvs[mesh.triangles[i + 1]].x * texture.width), (int)(uvs[mesh.triangles[i + 1]].y * texture.height));
            Vector2Int uv_c = new Vector2Int((int)(uvs[mesh.triangles[i + 2]].x * texture.width), (int)(uvs[mesh.triangles[i + 2]].y * texture.height));

            texture.SetPixel(uv_a.x, uv_a.y, color_a);
            texture.SetPixel(uv_b.x, uv_b.y, color_b);
            texture.SetPixel(uv_c.x, uv_c.y, color_c);
        }

        texture.Apply();

        island_mat.mainTexture = texture;

        GetComponent<MeshFilter>().sharedMesh = mesh;

        GetComponent<MeshRenderer>().sharedMaterial = island_mat;
    }
}