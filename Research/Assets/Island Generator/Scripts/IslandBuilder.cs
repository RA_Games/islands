﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class IslandBuilder : MonoBehaviour
{
    public enum Resolutions
    {
        X256 = 256, X512 = 512
    }

    public Resolutions resolution;

    [Range(0, 1)] public float size = 1;
    [Range(0, 1)] public float accidentFactor = 1;
    [Range(2, 100)] public float maxLayers = 2;
}
