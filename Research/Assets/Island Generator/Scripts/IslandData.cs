﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class IslandData
{
    public float celcius;
    public float height;

    [SerializeField] float temperature;
    [SerializeField] float accidentFactor;
    [SerializeField] float humidity;
    [SerializeField] float size;
    [SerializeField] float flora;
    [SerializeField] float fauna;
    [SerializeField] float inteligence;
    [SerializeField] float ores;
    [SerializeField] float ruins;
    [SerializeField] float anomalies;
    [SerializeField] public float yaw;
    [SerializeField] public float pitch;

    [SerializeField] Vector3 worldPos;

    public float Temperature
    {
        get
        {
            return temperature;
        }
    }

    public float AccidentFactor
    {
        get
        {
            return accidentFactor;
        }
    }

    public float Humidity
    {
        get
        {
            return humidity;
        }
    }

    public float Size
    {
        get
        {
            return size;
        }
    }

    public float Flora
    {
        get
        {
            return flora;
        }
    }

    public float Fauna
    {
        get
        {
            return fauna;
        }
    }

    public float Inteligence
    {
        get
        {
            return inteligence;
        }
    }

    public float Ores
    {
        get
        {
            return ores;
        }
    }

    public float Ruins
    {
        get
        {
            return ruins;
        }
    }

    public float Anomalies
    {
        get
        {
            return anomalies;
        }
    }

    public IslandData(float temperature, float accidentFactor, float humidity, float size, float flora, float fauna, float inteligence, float ores, float ruins, float anomalies, float yaw, float pitch)
    {
        this.temperature = temperature;
        this.accidentFactor = accidentFactor;
        this.humidity = humidity;
        this.size = size;
        this.flora = flora;
        this.fauna = fauna;
        this.inteligence = inteligence;
        this.ores = ores;
        this.ruins = ruins;
        this.anomalies = anomalies;
        this.yaw = yaw;
        this.pitch = pitch;

        celcius = CalculateTemperatureInCelcius(this.temperature);
        height = CalculateHeightWithTemperature(this.celcius);

        worldPos = new Vector3(0,height,0);
    }

    private float CalculateTemperatureInCelcius(float temperature)
    {
        return 200 * Mathf.Log((111111111 * temperature) + 0.606530659f);
    }

    private float CalculateHeightWithTemperature(float temperatureInCelcius)
    {
        float termicConstant = 5.861380564f;
        float part1 = temperatureInCelcius - 3700;
        float part2 = -Mathf.Exp(termicConstant);

        double part3 = -((temperatureInCelcius - 3700) * -Mathf.Exp(termicConstant));
        return Mathf.Clamp(Mathf.Exp(((int)part3) / 100), 3700, 50000);//Mathf.Exp(-((temperatureInCelcius - 3700) * -Mathf.Exp(termicConstant)));
    }
}
