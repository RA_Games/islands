﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class IslandHeightData : ScriptableObject
{
    public Mesh topology_mesh;

    [System.Serializable]
    public struct Layer
    {
        public int height;
        public Texture2D layer;

        public Layer(Texture2D layer, int height)
        {
            this.height = height;
            this.layer = layer;
        }
    }

    [SerializeField]
    private Layer[] topLayers;
    [SerializeField]
    private Layer[] bottomLayers;

    public Layer[] GetTopLayers()
    {
        return topLayers;
    }

    public Layer[] GetBottomLayers()
    {
        return bottomLayers;
    }

    public void SetTopLayers(Texture2D[] layers)
    {
       // Texture2D heightMap = TextureGenerator.Solid(layers[0].width, layers[0].height, Color.black);

        //float opacity = 1 / layers.Length;

        this.topLayers = new Layer[layers.Length];

        for (int i = 0; i < layers.Length; i++)
        {
            this.topLayers[i] = new Layer(layers[i], i);
        }

            /* for (int i = 0; i < layers.Length; i++)
             {
                 this.layers[i] = new Layer(layers[i], i);

                 for (int y = 0; y < layers[i].height; y++)
                 {
                     for (int x = 0; x < layers[i].width; x++)
                     {
                         Color color = layers[i].GetPixel(x,y);
                         color.a = opacity;

                         heightMap.SetPixel(x,y,color);
                     }
                 }
             }*/

#if UNITY_EDITOR
        //    System.IO.File.WriteAllBytes(Application.dataPath + "/Output.png", heightMap.EncodeToPNG());
      //  UnityEditor.AssetDatabase.Refresh();
    }
#endif

    public void SetBottomLayers(Texture2D[] layers)
    {
        this.bottomLayers = new Layer[layers.Length];

        for (int i = 0; i < layers.Length; i++)
        {
            this.bottomLayers[i] = new Layer(layers[i], i);
        }
    }
}